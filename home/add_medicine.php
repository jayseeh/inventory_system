
<?php 
  include "connect.php";
?>
<!DOCTYPE html>
<html class="st-layout ls-top-navbar-large ls-bottom-footer show-sidebar sidebar-l3" lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Inventory</title>
  <link href="css/vendor/all.css" rel="stylesheet">
  <link href="css/app/app.css" rel="stylesheet">
</head>

<body>

  <!-- Wrapper required for sidebar transitions -->
  <div class="st-container">

    <!-- NAVBAR HERE -->
    <?php include('includes/navbar.php') ?>
    <!-- END NAVBAR -->

    <!-- content push wrapper -->
    <div class="st-pusher" id="content">

      <!-- sidebar effects INSIDE of st-pusher: -->
      <!-- st-effect-3, st-effect-6, st-effect-7, st-effect-8, st-effect-14 -->

      <!-- this is the wrapper for the content -->
      <div class="st-content">

        <!-- extra div for emulating position:fixed of the menu -->
        <div class="st-content-inner padding-none">

          <div class="container-fluid">

            <div class="page-section">
              <div class="media v-middle">
                <div class="media-body">
                  <h1 class="text-display-1 margin-none">Add Medicine in Inventory</h1>
                </div>
              </div>
            </div>
              <div class="panel panel-default">
                <div class="panel-body">
                  <form action="sql/add.php" method="POST">
                    <!--<div class="form-group form-control-material static required">
                      <input type="text" class="form-control" id="idnumber" placeholder="ID Number (10 character max)" name="idnumber" maxlength="10">
                      <label for="idnumber">ID Number</label>
                    </div> -->
                    <div class="form-group form-control-material static required">
                      <input type="text" class="form-control" id="brand_name" placeholder="Brand_Name" name="brand_name">
                      <label for="name">Brand_Name</label>
                    </div>
                    <div class="form-group form-control-material static required">
                      <input type="text" class="form-control" id="generic_name" placeholder="Generic_Name" name="generic_name">
                      <label for="name">Generic_Name</label>
                    </div>
                    <div class="form-group form-control-material static required">
                      <input type="text" class="form-control" id="grams" placeholder="Dosage" name="grams">
                      <label for="name">Dosage</label>
                    </div>
                    <div class="form-group form-control-material static required">
                      <input type="text" class="form-control" id="qty" placeholder="QTY" name="qty">
                      <label for="name">QTY</label>
                    </div>
                    <button type="submit" class="btn btn-primary" name="add">Submit</button>
                  </form>
                </div>
              </div>

          </div>

        </div>
        <!-- /st-content-inner -->

      </div>
      <!-- /st-content -->

    </div>
    <!-- /st-pusher -->

    <!-- Footer -->

    <!-- // Footer -->

  </div>
    <!-- /st-pusher -->

    <!-- Footer -->
    <?php include('includes/footer.php'); ?>
    <!-- // Footer -->

  </div>
  <!-- /st-container -->

  <!-- Inline Script for colors and config objects; used by various external scripts; -->
  <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "html",
      skins: {
        "default": {
          "primary-color": "#42a5f5"
        }
      }
    };
  </script>
  <script src="js/vendor/all.js"></script>

  <script src="js/app/app.js"></script>


</body>

</html>