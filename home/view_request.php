<?php 
  include "connect.php";
?>
<!DOCTYPE html>
<html class="st-layout ls-top-navbar-large ls-bottom-footer show-sidebar sidebar-l3" lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Inventory</title>
  <link href="css/vendor/all.css" rel="stylesheet">
  <link href="css/app/app.css" rel="stylesheet">
</head>

<body>

  <!-- Wrapper required for sidebar transitions -->
  <div class="st-container">

    <!-- NAVBAR HERE -->
    <?php include('includes/navbar.php') ?>
    <!-- END NAVBAR -->

    <!-- content push wrapper -->
    <div class="st-pusher" id="content">

      <!-- sidebar effects INSIDE of st-pusher: -->
      <!-- st-effect-3, st-effect-6, st-effect-7, st-effect-8, st-effect-14 -->

      <!-- this is the wrapper for the content -->
      <div class="st-content">

        <!-- extra div for emulating position:fixed of the menu -->
        <div class="st-content-inner padding-none">

          <div class="container-fluid">

            <div class="page-section">
              <div class="media v-middle">
                <div class="media-body">
                  <h1 class="text-display-1 margin-none">View Requests</h1>
                </div>
              </div>
            </div>
          <div class="panel panel-default">
            <!-- Data table -->
            <table data-toggle="data-table" class="table" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Staff Name</th>
                  <th>Brand</th>
                  <th>Generic Name</th>
                  <th>Dosage</th>
                  <th>QTY</th>
                  <th>Date Requested</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Staff Name</th>
                  <th>Brand</th>
                  <th>Generic Name</th>
                  <th>Dosage</th>
                  <th>QTY</th>
                  <th>Date Requested</th>
                </tr>
              </tfoot>
              <tbody> 
                <?php
                  $query = mysqli_query($connect,"SELECT * FROM distribution");
                  while ($row = mysqli_fetch_assoc($query)) {
                    echo "<tr>";
                    echo "<td>".$row['id']."</td>";
                    echo "<td>".$row['staff_name']."</td>";
                    echo "<td>".$row['medicine_brand']."</td>";
                    echo "<td>".$row['medicine_name']."</td>";
                    echo "<td>".$row['dosage']."</td>";
                    echo "<td>".$row['qty']."</td>";
                    echo "<td>".$row['date_requested']."</td>";
                    echo "</tr>";
                  }
                ?>
                
              </tbody>
            </table>
            <!-- // Data table -->
          </div>

          </div>

        </div>
        <!-- /st-content-inner -->

      </div>
      <!-- /st-content -->

    </div>
    <!-- /st-pusher -->

    <!-- Footer -->
    <footer class="footer">
      <strong>Inventory</strong> v1.1.0 &copy; Copyright 2015
    </footer>
    <!-- // Footer -->

  </div>
    <!-- /st-pusher -->

    <!-- Footer -->
    <?php include('includes/footer.php'); ?>
    <!-- // Footer -->

  </div>
  <!-- /st-container -->

  <!-- Inline Script for colors and config objects; used by various external scripts; -->
  <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "html",
      skins: {
        "default": {
          "primary-color": "#42a5f5"
        }
      }
    };
  </script>
  <script src="js/vendor/all.js"></script>

  <script src="js/app/app.js"></script>


</body>

</html>