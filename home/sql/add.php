<?php
include "../connect.php";
date_default_timezone_set("Asia/Singapore");

if(isset($_POST['add'])){
	//$idnum = $_POST['idnumber'];
	//$id_hash = md5($idnum);
	$brand_name = $_POST['brand_name'];
	$generic_name = $_POST['generic_name'];
	$grams = $_POST['grams'];
	$qty = $_POST['qty'];
	$datee = date("Y-m-d h:i:sa");
	$new_qty = 0;
	if(($brand_name != null) && ($generic_name != null) && ($grams != null) && ($qty != null)){

		$query = mysqli_query($connect, "INSERT INTO added_medicines_record(brand_name,generic_name,qty,dosage,date_added) VALUES ('$brand_name','$generic_name','$qty','$grams','$datee')");
		if($query){
			$search = mysqli_query($connect,"SELECT * 
				FROM list_medicines 
				WHERE brand_name = '$brand_name' and
					generic_name = '$generic_name' and
					dosage = '$grams'
				");
			if(mysqli_num_rows($search) >= 1){
				//update qty
				$data = mysqli_fetch_assoc($search);
				$new_qty = $data['available_qty'] + $qty;
				$query3 = mysqli_query($connect,"UPDATE list_medicines 
					SET available_qty='$new_qty'	
					WHERE brand_name = '$brand_name' and
					generic_name = '$generic_name' and
					dosage = '$grams'");
				if(!$query3){
					echo "<script>";
					echo "alert('ERROR1');";
					echo "window.location='../add_medicine.php';";
					echo "</script>";
				}else {
					echo "<script>";
					echo "alert('Success');";
					echo "window.location='../add_medicine.php';";
					echo "</script>";
				}
			}
			else{
				//insert new record
				$query4 = mysqli_query($connect,"INSERT INTO list_medicines(brand_name,generic_name,available_qty,dosage) VALUES ('$brand_name','$generic_name','$qty','$grams')");
				if(!$query4){
					echo "<script>";
					echo "alert('ERROR2');";
					echo "window.location='../add_medicine.php';";
					echo "</script>";
				}else {
					echo "<script>";
					echo "alert('Success');";
					echo "window.location='../add_medicine.php';";
					echo "</script>";
				}
			}
		}
		if(!$query){
			echo "<script>";
			echo "alert('ERROR3');";
			echo "window.location='../add_medicine.php';";
			echo "</script>";
		}
	}
	else {
		echo "<script>";
		echo "alert('ERROR4');";
		echo "window.location='../add_medicine.php';";
		echo "</script>";
	}
}