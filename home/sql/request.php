<?php
include "../connect.php";
date_default_timezone_set("Asia/Singapore");

if(isset($_POST['request'])){
	//$idnum = $_POST['idnumber'];
	//$id_hash = md5($idnum);
	$staff = $_POST['staff_name'];
	$medicine_brand = $_POST['medicine_brand'];
	$medicine_name = $_POST['medicine_name'];
	$grams = $_POST['grams'];
	$qty = $_POST['qty'];
	$datee = date("Y-m-d h:i:sa");
	$new_qty = 0;
	if(($medicine_brand != null) && ($medicine_name != null) && ($grams != null) && ($qty != null)){
		//search medicine if in list to view available qty
		$search = mysqli_query($connect,"SELECT * 
			FROM list_medicines 
			WHERE brand_name = '$medicine_brand' and
			generic_name = '$medicine_name' and
			dosage = '$grams'
		");
		if(mysqli_num_rows($search) >= 1){
			//check if available qty is sufficient
			$data = mysqli_fetch_assoc($search);
			if($data['available_qty'] >= $qty){
				$insert = mysqli_query($connect,"INSERT INTO distribution (staff_name,medicine_brand,medicine_name,qty,dosage,date_requested) VALUES ('$staff','$medicine_brand','$medicine_name','$qty','$grams','$datee')");
				if($insert){
					//update qty in list_medicines
					$new_qty = $data['available_qty'] - $qty;

					$update = mysqli_query($connect,"UPDATE list_medicines 
						SET available_qty='$new_qty'	
						WHERE brand_name = '$medicine_brand' and
						generic_name = '$medicine_name' and
						dosage = '$grams'");
					if($update){
						echo "<script>";
						echo "alert('Success!');";
						echo "window.location='../request_medicine.php';";
						echo "</script>";
					}
					else{
						echo "<script>";
						echo "alert('Error update sql');";
						echo "window.location='../request_medicine.php';";
						echo "</script>";
					}
				}
				else{
					echo "<script>";
					echo "alert('Error insert sql');";
					echo "window.location='../request_medicine.php';";
					echo "</script>";
				}
			}
			else {
				echo "<script>";
				echo "alert('Insuficient Qty  ');";
				echo "window.location='../request_medicine.php';";
				echo "</script>";
			}
		}

	}
	else {
		echo "<script>";
		echo "alert('null fields');";
		echo "window.location='../request_medicine.php';";
		echo "</script>";
	}
}