
<?php 
  include "connect.php";
?>
<!DOCTYPE html>
<html class="st-layout ls-top-navbar-large ls-bottom-footer show-sidebar sidebar-l3" lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Inventory</title>
  <link href="css/vendor/all.css" rel="stylesheet">
  <link href="css/app/app.css" rel="stylesheet">
</head>

<body>

  <!-- Wrapper required for sidebar transitions -->
  <div class="st-container">

    <!-- NAVBAR HERE -->
    <?php include('includes/navbar.php') ?>
    <!-- END NAVBAR -->

    <!-- content push wrapper -->
    <div class="st-pusher" id="content">

      <!-- sidebar effects INSIDE of st-pusher: -->
      <!-- st-effect-3, st-effect-6, st-effect-7, st-effect-8, st-effect-14 -->

      <!-- this is the wrapper for the content -->
      <div class="st-content">

        <!-- extra div for emulating position:fixed of the menu -->
        <div class="st-content-inner padding-none">

          <div class="container-fluid">

            <div class="page-section">
              <div class="media v-middle">
                <div class="media-body">
                  <h1 class="text-display-1 margin-none">Request Medicine</h1>
                </div>
              </div>
            </div>
              <div class="panel panel-default">
                <div class="panel-body">
                  <form action="sql/request.php" method="POST">
                    <!--<div class="form-group form-control-material static required">
                      <input type="text" class="form-control" id="idnumber" placeholder="ID Number (10 character max)" name="idnumber" maxlength="10">
                      <label for="idnumber">ID Number</label>
                    </div> -->
                    <div class="form-group form-control-material static required">
                      <input type="text" class="form-control" id="staff_name" placeholder="Staff_name" name="staff_name">
                      <label for="name">Staff_name</label>
                    </div>
                    <div class="form-group form-control-material static required">
                      <?php 
                        $query = mysqli_query($connect,"SELECT DISTINCT generic_name FROM list_medicines");
                      ?>
                      <select class="form-control" id="gname" name="medicine_name">
                        <option>SELECT HERE</option>
                        <?php
                          while($row=mysqli_fetch_assoc($query)){
                            echo "<option value='".$row['generic_name']."'>".$row['generic_name']."</option>";
                          }

                        ?>
                      </select>
                      <!-- <input type="text" class="form-control" id="medicine_name" placeholder="Medicine_name" name="medicine_name"> -->
                      <label for="name">Generic_name</label>
                    </div>
                    <div class="form-group form-control-material static required">
                      <select class="form-control" id='mname' name="medicine_brand">
                      </select>
                      <label for="medicine_brand">Brand</label>
                    </div>
                    <div class="form-group form-control-material static required">
                      <select class="form-control" id='grams' name="grams"></select>
                      <!-- <input type="text" class="form-control" id="grams" placeholder="grams" name="grams"> -->
                      <label for="name">Dosage</label>
                    </div>
                    <div class="form-group form-control-material static required">
                      <input type="text" class="form-control" id="qty" placeholder="QTY" name="qty">
                      <label for="name">QTY</label>
                    </div>
                    <button type="submit" class="btn btn-primary" name="request">Request</button>
                  </form>
                </div>
              </div>

          </div>

        </div>
        <!-- /st-content-inner -->

      </div>
      <!-- /st-content -->

    </div>
    <!-- /st-pusher -->

    <!-- Footer -->

    <!-- // Footer -->

  </div>
    <!-- /st-pusher -->

    <!-- Footer -->
    <?php include('includes/footer.php'); ?>
    <!-- // Footer -->

  </div>
  <!-- /st-container -->

  <!-- Inline Script for colors and config objects; used by various external scripts; -->
  <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "html",
      skins: {
        "default": {
          "primary-color": "#42a5f5"
        }
      }
    };
  </script>
  <script src="js/jquery.min.js"></script>
  <script>
    $(document).ready(function(){
      $("#gname").change(function(){
        ref = $(this).val();
        console.log(ref);
        medname = "<option>----</option>";
        $.post("sql/filter.php",
        {
          condition: "generic_name='"+ref+"'",
          dataGet: 'brand_name'
        },
        function(data,status){
          console.log(data);
          var sample = data.split(" ");
          //console.log(sample);
          for (var i = sample.length - 1; i >= 0; i--) {
            console.log(sample[i]);
            medname = medname + "<option>"+sample[i]+"</option>";
          }
          $("#mname").html(medname);
/*            medname = medname + "<option></option>";*/
          //alert("Data: " + data + "\nStatus: " + status);
        });
      });
      $("#mname").change(function(){
        mname = $(this).val();
        gname = $("#gname").val();
        console.log(gname);
        medname = "<option>----</option>";
        $.post("sql/filter.php",
        {
          condition: "generic_name='"+gname+"' and brand_name='"+mname+"'",
          dataGet: "dosage"
        },
        function(data,status){
          console.log(data);
          var sample = data.split(" ");
          //console.log(sample);
          for (var i = sample.length - 1; i >= 0; i--) {
            console.log(sample[i]);
            medname = medname + "<option>"+sample[i]+"</option>";
          }
          $("#grams").html(medname);
/*            medname = medname + "<option></option>";*/
          //alert("Data: " + data + "\nStatus: " + status);
        });
      });
    });
  </script>
  <script src="js/vendor/all.js"></script>

  <script src="js/app/app.js"></script>


</body>

</html>