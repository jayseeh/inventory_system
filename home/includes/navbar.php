<!-- Fixed navbar -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  setInterval(function(){
    $.get("sql/MessageCount.php", function(data, status){
      //alert("Data: " + data + "\nStatus: " + status);
      $("#count").html("Messages "+data);
      $(".badge").html(data);
    });
    }, 1000);
});
</script>
    <div class="navbar navbar-size-large navbar-default navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <a href="#sidebar-menu" data-toggle="sidebar-menu" class="toggle pull-left visible-xs"><i class="fa fa-ellipsis-v"></i></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-brand navbar-brand-primary navbar-brand-logo navbar-nav-padding-left">
            <a  href="index.php" style="font-size: 12px;">
             Inventory
            </a>
          </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="main-nav">
          <ul class="nav navbar-nav navbar-nav-bordered navbar-right">
            <!-- notifications -->
            <!-- <li class="dropdown notifications updates">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="badge badge-primary"></span>
              </a>
              <ul class="dropdown-menu" role="notification">
                <li class="dropdown-header">Notifications</li>
                <li class="media">
                  <div class="pull-right">
                    <span class="label label-success">New</span>
                  </div>
                  <div class="media-left">
                    <img src="images/people/50/guy-2.jpg" alt="people" class="img-circle" width="30">
                  </div>
                  <div class="media-body">
                    <a href="#">Adrian D.</a> posted <a href="#">a photo</a> on his timeline.
                    <br/>
                    <span class="text-caption text-muted">5 mins ago</span>
                  </div>
                </li>
                <li class="media">
                  <div class="pull-right">
                    <span class="label label-success">New</span>
                  </div>
                  <div class="media-left">
                    <img src="images/people/50/guy-6.jpg" alt="people" class="img-circle" width="30">
                  </div>
                  <div class="media-body">
                    <a href="#">Bill</a> posted <a href="#">a comment</a> on Adrian's recent <a href="#">post</a>.
                    <br/>
                    <span class="text-caption text-muted">3 hrs ago</span>
                  </div>
                </li>
                <li class="media">
                  <div class="media-left">
                    <span class="icon-block s30 bg-grey-200"><i class="fa fa-plus"></i></span>
                  </div>
                  <div class="media-body">
                    <a href="#">Mary D.</a> and <a href="#">Michelle</a> are now friends.
                    <p>
                      <span class="text-caption text-muted">1 day ago</span>
                    </p>
                    <a href="#">
                      <img class="width-30 img-circle" src="images/people/50/woman-6.jpg" alt="people">
                    </a>
                    <a href="#">
                      <img class="width-30 img-circle" src="images/people/50/woman-3.jpg" alt="people">
                    </a>
                  </div>
                </li>
              </ul>
            </li> -->
            <!-- // END notifications -->
            <!-- User -->
           <!-- <li class="dropdown">
              <a href="#" class="dropdown-toggle user" data-toggle="dropdown">
                <img src="images/people/110/guy-5.jpg" alt="Bill" class="img-circle" width="40" /> Bill <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="app-student-profile.html">Account</a></li>
                <li><a href="app-student-billing.html">Billing</a></li>
                <li><a href="login.html">Logout</a></li>
              </ul>
            </li>-->
          </ul>
        </div>
        <!-- /.navbar-collapse -->

      </div>
    </div>

    <!-- Sidebar component with st-effect-1 (set on the toggle button within the navbar) -->
    <div class="sidebar left sidebar-size-3 sidebar-offset-0 sidebar-visible-desktop sidebar-visible-mobile sidebar-skin-dark" id="sidebar-menu" data-type="collapse">
      <div data-scrollable>

        <!-- <div class="sidebar-block">
          <div class="profile">
            <a href="profile.php">
              <?php 
                $avatar=$_SESSION['avatar'];
                $path = "../Images/".$avatar;
              ?>
              <img src="<?php echo $path; ?>" alt="people" class="img-circle width-80" />
            </a>
            <h4 class="text-display-1 margin-none">ADMIN</h4>
          </div>
        </div> -->

        <ul class="sidebar-menu">
          <!-- <li><a href="../admin_portal"><i class="fa fa-home"></i><span>Dashboard</span></a></li> -->
          <?php
           /* $ref = $_SESSION['id_hash'];
            $numbermessage = mysqli_query($connect,"SELECT * FROM messages WHERE receiver='$ref' and status='unread'");
            $number="";
            if(mysqli_num_rows($numbermessage) > 0 )
            $number="(".mysqli_num_rows($numbermessage).")";
*/
          ?>
         <!-- <li><a href="messages.php"><i class="fa fa-comments"></i><span id="count">Messages</p></span></a></li> -->
          <!-- <li><a href="app-instructor-messages.html"><i class="fa fa-mortar-board"></i><span>Instructors</span></a></li> -->
          <!-- <li><a href="app-instructor-messages.html"><i class="fa fa-mortar-board"></i><span>Students</span></a></li>
          <li><a href="register-school.php"><i class="fa fa-home"></i><span>Schools</span></a></li> -->
          <li>
            <a href="index.php"><i class="fa fa-file-text-o"></i><span>Home</span></a>
          </li>
          <li class="hasSubmenu">
            <a href="#instructor-menu"><i class="fa fa-file-text-o"></i><span>Inventory</span></a>
            <ul id="instructor-menu">
              <li><a href="add_medicine.php"><span>Add</span></a></li>
              <li><a href="view_medicines.php"><span>View Available Medicines</span></a></li>
              <li><a href="view_added_medicines.php"><span>View Added Medicines</span></a></li>
            </ul>
          </li>
          <li class="hasSubmenu">
            <a href="#Students-menu"><i class="fa fa-file-text-o"></i><span>Distribute</span></a>
            <ul id="Students-menu">
               <li><a href="request_medicine.php"><span>New Request</span></a></li>
              <li><a href="view_request.php"><span>View Requests</span></a></li>
            </ul>
          </li>
          <!--<li class="hasSubmenu">
            <a href="#schools-menu"><i class="fa fa-home"></i><span>Schools</span></a>
            <ul id="schools-menu">
              <li><a href="insert-schools.php" ><span>Insert</span></a></li>
              <li><a href="view-schools.php"><span>View</span></a></li>
            </ul>
          </li> -->
          <!--<li><a href="app-instructor-courses.html"><i class="fa fa-mortar-board"></i><span>My Courses</span></a></li> 
          <li><a href="app-instructor-earnings.html"><i class="fa fa-bar-chart-o"></i><span>Earnings</span></a></li>
          <li><a href="app-instructor-statement.html"><i class="fa fa-dollar"></i><span>Statement</span></a></li> -->
          <!--<li><a href="../../learning_system"><i class="fa fa-sign-out"></i><span>Logout</span></a></li> -->
        </ul>
      </div>
    </div>