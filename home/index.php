<?php session_start(); ?>
<!DOCTYPE html>
<html class="st-layout ls-top-navbar-large ls-bottom-footer show-sidebar sidebar-l3" lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Inventory</title>
  <link href="css/vendor/all.css" rel="stylesheet">
  <link href="css/app/app.css" rel="stylesheet">
</head>

<body>

  <div class="st-container">

    <!-- Fixed navbar -->
    <?php 
    include "connect.php";
    include('includes/navbar.php'); ?>
    
    <!--end sidebar -->
    <div class="st-pusher" id="content">
      <div class="st-content">
        <div class="st-content-inner padding-none">
          <div class="container-fluid">
            <div class="page-section">
              <div class="media v-middle">
                <div class="media-body">
                  <h1 class="text-display-1 margin-none">View Available Medicine List</h1>
                </div>
              </div>
            </div>
              <div class="panel panel-default">
                <!-- Data table -->
                <table data-toggle="data-table" class="table" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Brand</th>
                      <th>Generic Name</th>
                      <th>Dosage</th>
                      <th>Available QTY</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>Brand</th>
                      <th>Generic Name</th>
                      <th>Dosage</th>
                      <th>Available QTY</th>
                    </tr>
                  </tfoot>
                  <tbody> 
                    <?php
                      $query = mysqli_query($connect,"SELECT * FROM list_medicines");
                      while ($row = mysqli_fetch_assoc($query)) {
                        echo "<tr>";
                        echo "<td>".$row['id']."</td>";
                        echo "<td>".$row['brand_name']."</td>";
                        echo "<td>".$row['generic_name']."</td>";
                        echo "<td>".$row['dosage']."</td>";
                        echo "<td>".$row['available_qty']."</td>";
                        echo "</tr>";
                      }
                    ?>
                    
                  </tbody>
                </table>
                <!-- // Data table -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <?php include('includes/footer.php'); ?>
    <!-- // Footer -->
  </div>
  <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "html",
      skins: {
        "default": {
          "primary-color": "#42a5f5"
        }
      }
    };
  </script>
  <script src="js/vendor/all.js"></script>
  <script src="js/app/app.js"></script>
</body>
</html>